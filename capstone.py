
from abc import ABC


class Person(ABC):
	def __init__(self,firstName,lastName,email,department):
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

		# getter
	def getFirstName(self):
		return f"{self._firstName}"
	def getLastName(self):
		return f"{self._lastName}"
	def getFullName(self):
		return f"{self._firstName} {self._lastName}"

		# setter
	def setFirstName(self,new_firstName):
		self._firstName = new_firstName
	def setLastName(self,new_lastName):
		self._lastName = new_lastName

		# methods
	def login(self):
		return(f"{self._email} has logged in")
	def logout(self):
		return(f"{self._email} has logged out")


	def addRequest(self):
		return "Request has been added!"
	def checkRequest(self):
		pass
	def addUser(self):
		pass


class Employee(Person):
	def __init__(self,firstName,lastName,email,department):
		super().__init__(firstName, lastName, email, department) #to invoke immediate parent class constructor and methods(Person)
		

class TeamLead(Person):
	def __init__(self,firstName,lastName,email,department):
		super().__init__(firstName, lastName, email, department) 

		self._members = []


		# methods
	def addMember(self,Employee):
		self._members.append(Employee)

		#getter
	def get_members(self):
		return self._members
	# def set_members(self):
	# 	self._members = []



class Admin(Person):
	def __init__(self,firstName,lastName,email,department):
		super().__init__(firstName, lastName, email, department) 

		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department
		

		# methods
	def addUser(self):
		return(f"User has been added!")




class Request():
	def __init__(self,name,requester,dateRequested):

		self._name = name
		self._requester = requester
		self._dateRequested = dateRequested
		self._status = "Open"

	def updateRequest(self):
		return f"Request {self._name} has been updated"
	def closeRequest(self):
		return f"Request {self._name} has been closed"
	def cancelRequest(self):
		return f"Request {self._name} has been cancelled"

		# getter
	def get_status(self):
		return(f"{self._status}")

		# setter
	def set_status(self,new_status):
		self._status = new_status





# Instantiation
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")

req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")


print(employee1. getFullName())
print(admin1. getFullName())
print(teamLead1. getFullName())
print(employee2.login())
print(employee2.addRequest())
print(employee2.logout())


assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added!"
assert employee2.logout() == "sjane@mail.com has logged out"


teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
print(teamLead1.get_members())


for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added!"
print(admin1.addUser())

req2.set_status("Closed")
print(req2.closeRequest())
# print(req2.get_status())
